import React, { Component } from 'react';
import { default as Video, Controls, Play, Mute, Seek, Fullscreen, Time, Overlay } from 'react-html5video';
import $ from 'jquery';
import { Grid, Row, Col } from 'react-bootstrap';
import 'react-html5video/dist/ReactHtml5Video.css';
import './App.css';
import config from './env';

const videoNames = {
    0: 'PepsiCo_beverage_anim.mp4',
    1: 'PepsiCo_nutrition_anim.mp4'
};

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            volume: 1,
            test: false,
            ended: false,
            videoDimensions: this.getVideoDimensions(),
            videos: {},
            video: {
                id: config.defaultVideo,
                playing: true
            },
            testBlocks: {
                0: false,
                1: false,
                2: false,
                3: false,
                4: false,
                5: false
            }
        }
    }

    clearTest() {
        let testBlocks = {
            0: false,
            1: false,
            2: false,
            3: false,
            4: false,
            5: false
        };
        this.setState({testBlocks});
    }

    componentDidMount() {
        $(window).on('resize', () => this.windowResize());

        this.attachProgressBar();
    }

    attachProgressBar() {
        $('.video-progress-bar__input').on('click', (ev) => {
            $("video.video__el")[0].currentTime = parseInt(ev.target.value, 10);
        });
    }

    componentWillUnmount() {
        $(window).off('resize', () => this.windowResize());
    }

    windowResize() {
        this.setState({videoDimensions: this.getVideoDimensions()});
    }

    getVideoDimensions() {
        let videoDimensions = {
            width: 1920,
            height: 1080
        };

        if ((window.innerWidth / window.innerHeight) > (videoDimensions.width / videoDimensions.height)) {
            return {
                width: (window.innerHeight / videoDimensions.height) * videoDimensions.width,
                height: window.innerHeight
            };
        } else {
            return {
                width: window.innerWidth,
                height: (window.innerWidth / videoDimensions.width) * videoDimensions.height
            };
        }
    }

    ended() {
        let video = this.state.video;
        video.playing = false;

        this.setState({ended: true, video});
    }

    test() {
        this.setState({test: true});
    }

    changeVideo() {
        let video = this.state.video;
        video.id = video.id === 0 ? 1 : 0;
        video.playing = true;
        this.setState({ended: false, video}, () => {
            setTimeout(() => {
                this.clearTest();
                this.attachProgressBar()
            }, 300);
        });
    }

    rePlayVideo() {
        let video = this.state.video;
        video.playing = true;
        this.setState({ended: false, video}, () => {
            setTimeout(() => {
                this.attachProgressBar()
            }, 300);
        });
    }

    testColClick(colnum) {
        let testBlocks = this.state.testBlocks;
        testBlocks[colnum] = true;
        this.setState({testBlocks});
    }

    getTestCol(colnum) {
        return (
            <Col xs={6} sm={4}>
                <img role="button" alt={ "image_" + colnum } className="img-responsive img-option" src={document.baseURI + 'assets/images/test/' + this.state.video.id + '_' + colnum + '_' + (this.state.testBlocks[colnum] ? '1.png' : '0.png')} onClick={() => this.testColClick(colnum)} />
            </Col>
        );
    }

    getShareLink() {
        return this.state.video.id === 0 ?
            `mailto:?Subject=Beverage Transformation video&body=I thought you might like this video on how PepsiCo is transforming its Beverage business.%0A${document.baseURI}%0AEnjoy!`
            : `mailto:?Subject=Growing our Nutrition business video&body=I thought you might like this video on how PepsiCo is growing its Nutrition business.%0A${document.baseURI}%0AEnjoy!`;
    }

    getOptions() {
        return (
            <div>
                <Grid style={{display: !this.state.test ? 'block' : 'none'}}>
                    <Row>
                        <Col xs={6}>
                            <img role="button" alt="test" className="img-responsive img-link img-responsive2 center-block" src={document.baseURI + 'assets/images/test.png'} onClick={() => this.test()} />
                        </Col>
                        <Col xs={6}>
                            <a target="_blank" href={this.getShareLink()}>
                                <img role="button" alt="share" className="img-responsive center-block" src={document.baseURI + 'assets/images/share.png'} />
                            </a>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={2} xsOffset={10}>
                            <img role="button" alt="watch again" className="img-responsive img-link" src={document.baseURI + 'assets/images/watch_again.png'} onClick={() => this.rePlayVideo()}/>
                        </Col>
                    </Row>
                </Grid>
                <Grid style={{display: this.state.test ? 'block' : 'none'}}>
                    <Row className="titleRow">
                        <Col xs={12}>
                            <img role="presentation" className="img-responsive img-title" src={document.baseURI + 'assets/images/test/' + this.state.video.id + '_title.png'} />
                        </Col>
                    </Row>
                    <Row className="optionsRow">
                        {this.getTestCol(0)}
                        {this.getTestCol(1)}
                        {this.getTestCol(2)}
                        {this.getTestCol(3)}
                        {this.getTestCol(4)}
                        {this.getTestCol(5)}
                    </Row>
                    <Row>
                        <Col xs={2} xsOffset={10}>
                            <img role="button" alt="back" className="img-responsive img-link" src={document.baseURI + 'assets/images/back.png'} onClick={() => {
                                this.setState({test: false}, () => this.clearTest());
                            }}/>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }

    getVideoName() {
        return videoNames[this.state.video.id];
    }

    render() {
        if (!this.state.ended) {
            return (
                <div className="Video">
                    <Video
                        className="centered"
                        controls={true}
                        autoPlay={true}
                        loop={false}
                        muted={false}
                        onEnded={() => this.ended()}
                        style={{
                            width: this.state.videoDimensions.width,
                            height: this.state.videoDimensions.height
                        }}
                    >
                        <source src={`${document.baseURI}assets/videos/${this.getVideoName()}`} type="video/mp4"/>
                        <Overlay />
                        <Controls>
                            <Play />
                            <Seek />
                            <Time />
                            <Mute />
                            <Fullscreen />
                        </Controls>
                    </Video>
                </div>
            );
        } else {
            return (
                <div className="optionsContainer centered" style={{
                    backgroundColor: this.state.video.id === 0 ? '#fdedb1' : '#2dc8e4'
                }}>
                    <div className="outer" >
                        <div className="middle">
                            {this.getOptions()}
                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default App;
